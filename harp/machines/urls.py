from django.urls import path

from . import views
from .views import cad_file_queue

app_name = 'machines'


urlpatterns = [
    path('', views.DashboardView.as_view(), name='dashboard'),
    path('queue/', views.QueueView.as_view(), name='queue'),
    path('queue/<int:pk>/delete', views.queue_delete, name='queue_delete'),
    path('queue/<int:pk>/up', views.queue_up, name='queue_up'),
    path('queue/<int:pk>/down', views.queue_down, name='queue_down'),
    path('queue/<int:pk>/repeat', views.queue_repeat, name='queue_repeat'),
    path('cad', views.CADFileViewAll.as_view(), name='cad'),
    path('cad/add', views.CADFileCreate.as_view(), name='cad_add'),
    path('cad/<int:pk>/', views.CADFileView.as_view(), name='cad_view'),
    path('cad/<int:pk>/edit', views.CADFileEditView.as_view(), name='cad_edit'),
    path('cad/<int:pk>/delete', views.cad_delete, name='cad_delete'),
    path('cad/<int:pk>/queue', views.cad_file_queue, name='cad_queue'),
    path('machines/', views.PrintersView.as_view(), name='machines'),
    path('machines/add', views.PrinterCreate.as_view(), name='machine_add'),
    path('machines/<int:pk>/delete', views.machine_delete, name='machine_delete'),
    path('machines/<int:pk>/clear_bed', views.mark_printer_cleared, name='clear_bed'),
    path('machines/<int:pk>/online', views.printer_offline_check, name='machine_online_checkbox'),
    path('external/pushover/add', views.PushoverCreate.as_view(), name='pushover_create')
]