import json

from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views import generic
from django.views.generic import CreateView, UpdateView, DetailView

from .models import Printer, Gcode, QueueItem, CADFile, PushoverCall
from .forms import AddPrinterForm

def get_next_queue_position():
    last_queue_item = QueueItem.objects.order_by('position').last()
    queue_position = 1

    if last_queue_item is not None:
        queue_position = last_queue_item.position + 1

    return queue_position

class DashboardView(generic.ListView):
    model = Printer
    template_name = 'machines/dashboard.html'
    context_object_name = 'printers'


class QueueView(generic.ListView):
    template_name = 'machines/queue.html'
    context_object_name = 'queue'

    def get_queryset(self):
        queue_compact()
        return QueueItem.objects.order_by('position')


def queue_delete(request, pk):
    q = QueueItem.objects.get(pk=pk)
    q.delete()

    return redirect('machines:queue')


def queue_repeat(request, pk):
    if request.method == 'POST':
        data = json.loads(request.body.decode('utf-8'))
        qi = QueueItem.objects.get(pk=pk)
        qi.amount_to_print = int(data['count'])
        qi.save()

    return JsonResponse({'hi':'hi'})


class PrintersView(generic.ListView):
    model = Printer
    template_name = 'machines/printers.html'
    context_object_name = 'printers'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['add_printer_form'] = AddPrinterForm()
        return context


class PrinterCreate(CreateView):
    model = Printer
    fields = ['name', 'octoprint', 'api_key', 'config', 'tags']

    def get_success_url(self):
        return reverse('machines:machines')


class PrinterUpdate(UpdateView):
    model = Printer
    fields = ['__all__']

def machine_delete(request, pk):
    machine = Printer.objects.get(pk=pk)
    machine.delete()

    return redirect('machines:machines')


class CADFileViewAll(generic.ListView):
    model = CADFile
    fields = ['__all__']
    context_object_name = 'cad_files'
    queryset = CADFile.objects.all().order_by('tags')


class CADFileCreate(CreateView):
    model = CADFile
    fields = ['name', 'file', 'file_type', 'machine_tags', 'tags']

    # This is called if the user clicks the Add and Queue button.
    def post(self, request):
        x = super(CADFileCreate, self).post(request)
        if request.POST.get('add_and_queue'):
            queue_position = get_next_queue_position()
            cad_file = CADFile.objects.get(pk=self.object.id)
            q = QueueItem(position=queue_position, cad_file=cad_file)
            q.save()

        return x

    def get_success_url(self):
        return reverse('machines:cad')


class CADFileEditView(UpdateView):
    model = CADFile
    fields = ['name', 'machine_tags', 'tags']
    template_name_suffix = '_edit'

    def get_success_url(self):
        return reverse('machines:cad')


class CADFileView(DetailView):
    model = CADFile
    fields = ['__all__']


class PushoverCreate(CreateView):
    model = PushoverCall
    fields = ['printer', 'token', 'user', 'body']

    def get_success_url(self):
        return reverse('machines:machines')



def cad_file_queue(request, pk):
    cad_file = CADFile.objects.get(pk=pk)
    
    queue_position = get_next_queue_position()

    q = QueueItem(position=queue_position, cad_file=cad_file)
    q.save()

    return redirect('machines:cad')


def cad_delete(request, pk):
    cad_file = CADFile.objects.get(pk=pk)
    cad_file.delete()

    return redirect('machines:cad')


def printer_add(request):
    form = AddPrinterForm(request.POST)

    try:
        form.save()
    except ValueError:
        print('bad')

    return redirect('machines:machines')


def printer_offline_check(request, pk):
    if request.method == "POST":
        printer = Printer.objects.get(pk=pk)
        if 'online' in request.POST:
            printer.online = True
        else:
            printer.online = False

        printer.save()
        return redirect('machines:machines')
    else:
        return HttpResponse(status=400)


def mark_printer_cleared(request, pk):
    Printer.objects.get(pk=pk).mark_printer_cleared()
    return redirect('machines:dashboard')


def queue_up(request, pk):
    qi: QueueItem = QueueItem.objects.get(pk=pk)
    qip: QueueItem = QueueItem.objects.get(position=(qi.position-1))

    if qip is None:
        qi.position = qi.position-1
        qi.save()
    else:
        qi_pos = qi.position
        qi.position = qi_pos-1
        qi.save()
        qip.position = qi_pos
        qip.save()

    return redirect('machines:queue')


def queue_down(request, pk):
    qi: QueueItem = QueueItem.objects.get(pk=pk)
    qip: QueueItem = QueueItem.objects.get(position=(qi.position+1))

    if qip is not None:
        qi_pos = qi.position
        qi.position = qi_pos + 1
        qi.save()
        qip.position = qi_pos
        qip.save()

    return redirect('machines:queue')


def queue_compact():
    open_spot = 1
    for qi in QueueItem.objects.all().order_by('position'):
        if qi.position > open_spot:
            qi.position = open_spot
            open_spot = open_spot + 1
            qi.save()
        else:
            open_spot = open_spot + 1


def clear_gcode():
    gc_none = Gcode.objects.all().exclude(parent_cad=None)

    for x in gc_none:
        x.delete()


def external_queue(request):
    if request.method == "POST":
        pass
