from django.contrib import admin

# Register your models here.
from .models import Printer, Gcode, QueueItem, CADFile, PushoverCall

admin.site.register(Printer)
admin.site.register(Gcode)
admin.site.register(QueueItem)
admin.site.register(CADFile)
admin.site.register(PushoverCall)