from django import forms
from django.forms import ModelForm, Form, FileField, FileInput

from .models import Gcode, Printer


class AddPrinterForm(ModelForm):
    class Meta:
        model = Printer
        fields = ['name', 'octoprint', 'api_key', 'tags']


