class PrinterGcodeNoFile(Exception):
    def __str__(self):
        return 'No file was sent to Octoprint.'


class PrinterGcodeWrongUploadLocation(Exception):
    def __str__(self):
        return 'File was attempted to be uploaded to the wrong location.'


class PrinterGcodeFileExists(Exception):
    def __str__(self):
        return "File already exists."


class PrinterGcodeWrongFiletype(Exception):
    def __str__(self):
        return "Uploaded file is not a gocde or stl file."


class Printer500Error(Exception):
    def __str__(self):
        return "Unknown Octoprint error."


class MachineUnavailable(Exception):
    def __str__(self):
        return "Machine is unable to start job."
