from http import HTTPStatus
from time import sleep

from django.test import TestCase, Client

from .tasks import task_check_every_printer_status, add_queued_items_to_printer
from .views import cad_file_queue, queue_delete
from .models import Printer, Gcode, QueueItem, CADFile

api_key = 'CBD281E4276D4547987C212CDE959DFB'


class PrinterTestCase(TestCase):
    def setUp(self) -> None:
        printer = Printer.objects.create(name='test',
                                         octoprint='http://localhost:5000',
                                         api_key=api_key,
                                         status=Printer.PrinterStatus.READY,
                                         tags=['test'])
        printer.save()

    def test_check_printer_status(self):
        printer = Printer.objects.get(name='test')
        sleep(30)
        x = printer.get_status()
        self.assertIsNotNone(x['state'])
        printer.set_status()
        self.assertEqual(printer.status, Printer.PrinterStatus.READY)

    def test_file_does_exist(self):
        printer = Printer.objects.get(name='test')
        gcode = Gcode.objects.create(file='test_data/square.gcode',
                                     machine=printer)
        gcode.save()
        printer.upload_new_gcode_file(gcode.pk)
        rv = printer.check_file_exists(gcode.pk)
        self.assertTrue(rv)

    def test_file_does_not_exist(self):
        printer = Printer.objects.get(name='test')
        gcode = Gcode.objects.create(file='test_data/circle.gcode',
                                     machine=printer)
        gcode.save()

        rv = printer.check_file_exists(gcode.pk)
        self.assertFalse(rv)

    def test_start_print(self):
        printer = Printer.objects.get(name='test')
        gcode = Gcode.objects.create(file='test_data/square.gcode',
                                     machine=printer)
        gcode.save()
        printer.start_print(gcode.pk)
        sleep(5)
        printer.set_status()
        printer = Printer.objects.get(pk=printer.pk)
        self.assertEqual(printer.status, printer.PrinterStatus.PRINTING)

    def test_cancel_print(self):
        printer = Printer.objects.get(name='test')
        gcode = Gcode.objects.create(file='test_data/square.gcode',
                                     machine=printer)
        gcode.save()
        printer.start_print(gcode.pk)
        sleep(5)
        printer.set_status()
        printer = Printer.objects.get(pk=printer.pk)
        self.assertEqual(printer.status, printer.PrinterStatus.PRINTING)
        printer.cancel_current_job()
        sleep(20)
        printer.set_status()
        printer = Printer.objects.get(pk=printer.pk)
        self.assertEqual(printer.status, printer.PrinterStatus.PRINT_DONE)


class QueueTestCase(TestCase):
    def setUp(self) -> None:
        cad_file = CADFile.objects.create(name='test',
                                          file='test_data/square.stl',
                                          machine_tags=['test'],
                                          tags=['test file'])
        cad_file.save()
        cad_file = CADFile.objects.create(name='test2',
                                          file='test_data/circle.stl',
                                          machine_tags=['test'],
                                          tags=['test file'])
        cad_file.save()
        printer = Printer.objects.create(name='test',
                                         octoprint='http://localhost:5000',
                                         api_key=api_key,
                                         status=Printer.PrinterStatus.READY,
                                         tags=['test'],
                                         config='test_data/config_ender3.ini')
        printer.save()

    def test_add_item_to_queue(self):
        cad_file = CADFile.objects.get(name='test')
        cad_file_queue(None, cad_file.pk)
        self.assertEqual(len(QueueItem.objects.all()), 1)

    def test_remove_item_from_queue(self):
        cad_file = CADFile.objects.get(name='test')
        cad_file_queue(None, cad_file.pk)
        self.assertEqual(len(QueueItem.objects.all()), 1)
        queue_delete(None, 1)
        self.assertEqual(len(QueueItem.objects.all()), 0)

    def test_print_two_items(self):
        c1 = CADFile.objects.get(name='test')
        c2 = CADFile.objects.get(name='test2')
        cad_file_queue(None, c1.pk)
        cad_file_queue(None, c2.pk)
        self.assertEqual(len(QueueItem.objects.all()), 2)
        printer = Printer.objects.get(name='test')
        printer.cancel_current_job()
        printer.mark_printer_cleared()
        sleep(5)
        task_check_every_printer_status()
        add_queued_items_to_printer()
        sleep(5)
        printer.cancel_current_job()
        sleep(20)
        self.assertEqual(len(QueueItem.objects.all()), 1)
        sleep(40)
        task_check_every_printer_status()
        add_queued_items_to_printer()
        self.assertEqual(len(QueueItem.objects.all()), 0)

class CADTestCase(TestCase):
    def test_get(self):
        response = self.client.get('/cad')

        self.assertEqual(response.status_code, HTTPStatus.OK)
    
    def test_add_stl_file(self):
        response = self.client.post(
            '/cad/add',
            data={})
