# Create your tasks here

import logging

from typing import List
from celery import shared_task

from .models import Printer, QueueItem, Gcode

@shared_task
def task_check_every_printer_status():
    printers = Printer.objects.all()

    for printer in printers:
        printer.set_status()


@shared_task
def add_queued_items_to_printer():
    # Get all ready printers.
    printers = Printer.objects.filter(status=Printer.PrinterStatus.READY).filter(online=True)

    for printer in printers:
        tags: List[str] = [x.name for x in printer.tags.all()]
        queue_item: QueueItem = QueueItem.objects\
            .filter(cad_file__machine_tags__name__in=tags)\
            .filter(currently_printing=False)\
            .order_by('position')\
            .distinct().first()

        if queue_item is not None:
            # Check is there is an existing generated gcode file.
            gc = Gcode.objects.filter(parent_cad=queue_item.cad_file).filter(machine=printer).first()

            if gc is None:  # No gcode exists, must be generated.
                if printer.config is None:  # The printer doesn't have a slicer, so move on.
                    continue

                name = printer.create_gcode(queue_item.cad_file)
                gc: Gcode = Gcode(file=name.name, machine=printer, parent_cad=queue_item.cad_file)
                gc.save()

            printer.start_print(gc.pk)

            queue_item.amount_to_print = queue_item.amount_to_print-1
            queue_item.save()
            queue_item = QueueItem.objects.get(pk=queue_item.pk)

            if queue_item.amount_to_print < 1:
                queue_item.delete()
            else:
                queue_item.position = QueueItem.objects.order_by('position').last().position+1
                queue_item.save()


