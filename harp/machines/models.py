import hashlib
import json
import os
import subprocess
import uuid

from io import IOBase
from json.decoder import JSONDecodeError
from datetime import timedelta
from shutil import copyfile

import requests
from django.db import models

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.timezone import now
from tagulous.models import TagField

from .errors import *

PRINTER_STATUS = '/api/printer'
FILES_UPLOAD = '/api/files/local'
JOB_STATUS = '/api/job'


class FileType(models.TextChoices):
    GCODE = 'Gcode'
    STL = 'STL'


class CADFile(models.Model):
    name = models.CharField(max_length=200)
    file = models.FileField(upload_to='cad/')
    file_type = models.CharField(max_length=100, choices=FileType.choices, default=FileType.STL)
    machine_tags = TagField()
    tags = TagField()


class Printer(models.Model):
    class PrinterStatus(models.TextChoices):
        PRINT_DONE = 'Print Done'
        READY = 'Ready'
        PRINTING = 'Printing'
        CLEARED = 'Clear Bed'
        OFFLINE = 'Offline'

    name = models.CharField(max_length=200)
    octoprint = models.URLField()
    api_key = models.CharField(max_length=64)
    status = models.CharField(max_length=100, choices=PrinterStatus.choices, default=PrinterStatus.PRINT_DONE)
    last_updated = models.DateTimeField('last checked', default=now)
    online = models.BooleanField(default=True)
    config = models.FileField(upload_to='printer_config/')
    eta = models.DateTimeField('eta', default=now)
    percent_done = models.FloatField(default=0)
    tags = TagField()

    def get_status(self):
        headers = {'X-Api-Key': self.api_key}
        r = requests.get(self.octoprint + PRINTER_STATUS, headers=headers)
        try:
            return json.loads(r.content)
        except JSONDecodeError:
            return 'offline'

    def set_status(self):
        x = self.get_status()

        if x == 'offline':
            self.last_updated = timezone.now()
            self.status = Printer.PrinterStatus.OFFLINE
            self.save()
            return

        ready = x['state']['flags']['ready']
        printing = x['state']['flags']['printing']
        current_status = self.status

        if printing:
            self.status = Printer.PrinterStatus.PRINTING
            self.set_print_eta()
        elif ready:
            if self.status == Printer.PrinterStatus.PRINTING:
                self.status = Printer.PrinterStatus.PRINT_DONE
        else:
            self.status = Printer.PrinterStatus.PRINT_DONE

        self.last_updated = timezone.now()

        if self.status == Printer.PrinterStatus.PRINT_DONE and current_status != self.status:
            pushovers = PushoverCall.objects.filter(printer=self.pk)

            for pushover in pushovers:
                pushover.send_message()

        self.save()

    def mark_printer_cleared(self):
        x = self.get_status()

        if x == 'offline' or x['state']['flags']['ready']:
            self.status = Printer.PrinterStatus.READY
            self.save()

    def upload_new_gcode_file(self, gcode_pk):
        gcode = Gcode.objects.get(pk=gcode_pk)

        headers = {'X-Api-Key': self.api_key}
        fle = {'file': open(gcode.file.path, 'rb'), 'filename': os.path.basename(gcode.file.name)}

        r = requests.post(self.octoprint + FILES_UPLOAD, headers=headers, files=fle)

        if r.status_code == 400:
            raise PrinterGcodeNoFile()
        elif r.status_code == 404:
            raise PrinterGcodeWrongUploadLocation()
        elif r.status_code == 409:
            raise PrinterGcodeFileExists()
        elif r.status_code == 415:
            raise PrinterGcodeWrongFiletype()
        elif r.status_code == 500:
            raise Printer500Error()

    def check_file_exists(self, gcode_pk):
        gcode = Gcode.objects.get(pk=gcode_pk)

        headers = {'X-Api-Key': self.api_key}
        r = requests.get(self.octoprint + FILES_UPLOAD + '/' + os.path.basename(gcode.file.name), headers=headers)

        if r.status_code == 404:
            return False

        remote_hash = json.loads(r.text)['hash']

        local_hash: str = hashlib.sha1(open(gcode.file.name, 'rb').read()).hexdigest()

        return remote_hash == local_hash

    def start_print(self, gcode_pk):
        gcode = Gcode.objects.get(pk=gcode_pk)

        if self.status == Printer.PrinterStatus.READY:
            if not self.check_file_exists(gcode_pk):
                self.upload_new_gcode_file(gcode_pk)

            headers = {'X-Api-Key': self.api_key}
            data = {'command': 'select', 'print': 'true'}
            r = requests.post(self.octoprint + FILES_UPLOAD + '/' + os.path.basename(gcode.file.name),
                              headers=headers, json=data)

            if r.status_code == 409:
                raise MachineUnavailable()

    def cancel_current_job(self):
        headers = {'X-Api-Key': self.api_key}
        data = {'command': 'cancel'}
        requests.post(self.octoprint + JOB_STATUS, headers=headers, json=data)

    def set_print_eta(self):
        headers = {'X-Api-Key': self.api_key}
        r = requests.get(self.octoprint + JOB_STATUS, headers=headers)
        r = json.loads(r.content)
        seconds = r['progress']['printTimeLeft']
        percent = r['progress']['completion']

        try:
            self.eta = timezone.now() + timedelta(seconds=seconds)
        except TypeError:
            # Essentially the eta has not returned a number.
            self.eta = timezone.now()

        # Percent will be None if there isn't a print going on.
        if percent is not None:
            self.percent_done = percent
        else:
            self.percent_done = 0

        self.save()

    def create_gcode(self, file: CADFile) -> IOBase:
        """
        Returns a filehandle of a generated gcode file.
        """
        if file.file_type == FileType.STL.name:
            try:
                stl = file.file.path
                # generate unique name for the gcode
                name = 'gcode/' + str(uuid.uuid1()) + '.gcode'
                subprocess.check_output(['./PrusaSlicer.AppImage',
                                         '--load', self.config.path,
                                         '--export-gcode',
                                         '--output', name,
                                         stl])
                return open(name, 'r+')
            except subprocess.CalledProcessError as e:
                print(e.output)
        elif file.file_type == FileType.GCODE:
            try:
                # We cut off the first 4 characters of file.file.name
                # because it includes the cad/ directory in the name.
                return open(copyfile(file.file.path, 'gcode/' + file.file.name[4:]), 'r+')
            except Exception as e:
                print(e.output)

    def __str__(self):
        return self.name


class Gcode(models.Model):
    """
    This represents a base level file that goes directly to a machine.
    :file: Saved gcode file.
    :machine: Specific machine this gcode is tied to.
    :parent_cad: The originating CAD File that was used to generate this gcode file.
    """
    file = models.FileField(upload_to='gcode/')
    machine = models.ForeignKey(Printer, on_delete=models.CASCADE, null=True)
    parent_cad = models.ForeignKey(CADFile, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class QueueItem(models.Model):
    position = models.IntegerField()
    cad_file = models.ForeignKey(CADFile, on_delete=models.CASCADE)
    currently_printing = models.BooleanField(default=False)
    amount_to_print = models.IntegerField(default=1)

class PushoverCall(models.Model):
    printer = models.ForeignKey(Printer, on_delete=models.CASCADE)
    token = models.CharField(max_length=64)
    user = models.CharField(max_length=64)
    body = models.CharField(max_length=250)
    
    def send_message(self):
        data = {'token': self.token,
                'user': self.user,
                'message': self.body,
                'title': self.printer.name + ' | HARP'}

        res = requests.post('https://api.pushover.net/1/messages.json', data=data)
        print('----')
        print(res.json())

    @classmethod
    def post_create(cls, sender, instance, created, *args, **kwargs):
        if not created:
            return
        instance.send_message()

class Trigger(models.Model):
    printer = models.ForeignKey(Printer, on_delete=models.CASCADE)


@receiver(models.signals.post_delete, sender=Gcode)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem, when corresponding Gcode object is deleted.
    """
    if instance.file:
        if os.path.isfile(instance.file.path):
            os.remove(instance.file.path)

post_save.connect(PushoverCall.post_create, sender=PushoverCall)
