#!/bin/bash

trap 'kill %1; kill %2; kill %3' SIGINT

octoprint &
python manage.py runserver  &
celery -A harp beat -l WARNING --scheduler django_celery_beat.schedulers:DatabaseScheduler &
celery -A harp worker -l WARNING
