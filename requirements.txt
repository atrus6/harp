amqp==5.0.2
asgiref==3.3.1
Babel==2.9.0
billiard==3.6.3.0
blinker==1.4
cachelib==0.1.1
celery==5.0.5
certifi==2020.12.5
chardet==3.0.4
click==7.1.2
click-didyoumean==0.0.3
click-plugins==1.1.1
click-repl==0.1.6
Django==3.1.4
django-celery-beat==2.1.0
django-tagulous==1.1.0
django-timezone-field==4.1.1
emoji==0.6.0
feedparser==6.0.2
filetype==1.0.7
Flask==1.1.2
Flask-Assets==2.0
Flask-Babel==1.0.0
Flask-Login==0.5.0
frozendict==1.2
future==0.18.2
idna==2.10
ifaddr==0.1.7
itsdangerous==1.1.0
Jinja2==2.11.2
kombu==5.0.2
Markdown==3.1.1
MarkupSafe==1.1.1
netaddr==0.8.0
netifaces==0.10.9
OctoPrint==1.5.2
OctoPrint-FileCheck==2020.8.7
OctoPrint-FirmwareCheck==2020.9.23
pathtools==0.1.2
pkginfo==1.6.1
prompt-toolkit==3.0.8
psutil==5.8.0
pyasn1==0.4.8
pylru==1.2.0
pyserial==3.5
python-crontab==2.5.1
python-dateutil==2.8.1
pytz==2020.4
PyYAML==5.3.1
redis==3.5.3
regex==2020.11.13
requests==2.25.0
rsa==4.0
sarge==0.1.5.post0
semantic-version==2.8.5
sentry-sdk==0.19.5
sgmllib3k==1.0.0
six==1.15.0
sqlparse==0.4.1
tornado==5.1.1
Unidecode==0.4.21
urllib3==1.26.2
vine==5.0.0
watchdog==0.10.4
wcwidth==0.2.5
webassets==2.0
websocket-client==0.57.0
Werkzeug==1.0.1
wrapt==1.12.1
zeroconf==0.24.5
